defmodule OweAppBackend.Repo.Migrations.UserAddTokenIds do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :token_ids, {:array, :string}, default: []
    end
  end
end
