defmodule OweAppBackend.Repo.Migrations.CreateOwe do
  use Ecto.Migration

  def change do
    create table(:owes) do
      add :from_id, references(:users, column: :facebook_id, type: :string)
      add :to_id, references(:users, column: :facebook_id, type: :string)
      add :amount, :integer
      add :approved, :boolean, default: false

      timestamps
    end

  end
end
