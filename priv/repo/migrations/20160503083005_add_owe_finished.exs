defmodule OweAppBackend.Repo.Migrations.AddOweFinished do
  use Ecto.Migration

  def change do
    alter table(:owes) do
      add :finished, :boolean
    end
  end
end
