defmodule OweAppBackend.Repo.Migrations.CreateUserFriend do
  use Ecto.Migration

  def change do
    create table(:userfriends) do
      add :user_id, :string
      add :friend_id, :string

      timestamps
    end

  end
end
