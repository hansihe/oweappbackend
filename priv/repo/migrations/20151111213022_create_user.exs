defmodule OweAppBackend.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add :full_name, :string
      add :first_name, :string
      add :facebook_id, :string, primary_key: true
      add :email, :string
      add :access_token, :string

      timestamps
    end

  end
end
