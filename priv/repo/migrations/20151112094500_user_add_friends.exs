defmodule OweAppBackend.Repo.Migrations.UserAddFriends do
  use Ecto.Migration

  def change do

    alter table(:users) do
      add :friends, {:array, :map}, default: []
    end

  end
end
