defmodule OweAppBackend.OweTest do
  use OweAppBackend.ModelCase

  alias OweAppBackend.Owe

  @valid_attrs %{amount: 42, approved: true, from: 42, to: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Owe.changeset(%Owe{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Owe.changeset(%Owe{}, @invalid_attrs)
    refute changeset.valid?
  end
end
