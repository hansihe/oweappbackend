defmodule OweAppBackend.UserFriendTest do
  use OweAppBackend.ModelCase

  alias OweAppBackend.UserFriend

  @valid_attrs %{friend_id: "some content", user_id: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = UserFriend.changeset(%UserFriend{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = UserFriend.changeset(%UserFriend{}, @invalid_attrs)
    refute changeset.valid?
  end
end
