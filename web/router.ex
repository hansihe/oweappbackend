defmodule OweAppBackend.Router do
  use OweAppBackend.Web, :router

  import OweAppBackend.Util

  def resolve_token_user(token) do
    {:ok, claims} = OweAppBackend.Token.decode(token)
    "facebook_id|" <> facebook_id = pair_list_get(claims, "sub")
    OweAppBackend.Repo.get!(OweAppBackend.User, facebook_id)
  end

  def session_auth(conn, _) do
    facebook_id = get_session(conn, :facebook_id)
    case facebook_id do
      nil -> conn |> assign(:authed, false)
      facebook_id -> conn 
          |> assign(:authed, true) 
          |> assign(:user, OweAppBackend.Repo.get!(OweAppBackend.User, facebook_id))
    end
  end

  def api_auth(conn, _) do
    token = case Plug.Conn.get_req_header(conn, "authorization") do
      ["Bearer " <> token] -> token
      _ -> pair_list_get(conn.params, "access_token")
    end
    case token do
      nil -> session_auth(conn, [])
      tok -> conn |> assign(:authed, true) |> assign(:user, resolve_token_user(tok))
    end
  end

  def require_auth(conn, _) do
    case conn.assigns.authed do
      true -> conn
      false -> conn |> put_status(401) |> text("Not authorized") |> halt
    end
  end

  def api_response(conn, resp) do
    import Phoenix.Controller, only: [json: 2, text: 2]
    case pair_list_get(conn.params, "rt") do
      "lc_array" -> text conn, Base.encode64(IO.iodata_to_binary(LcArray.encode(resp)))
      _ -> json conn, resp
    end
  end

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :session_auth
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :api_auth
  end

  scope "/", OweAppBackend do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  scope "/login", OweAppBackend do
    pipe_through :browser

    get "/", LoginController, :index
    get "/facebook_return", LoginController, :facebook_return
  end

  scope "/lc_utils", OweAppBackend do
    pipe_through :browser

    get "/login_with_callback", LcUtilsController, :login_with_callback
    get "/login_with_callback_finished", LcUtilsController, :login_with_callback_finished
  end

  scope "/api", OweAppBackend do
    pipe_through :api

    resources "/tokens", TokenController
    resources "/owes", OweController
    post "/owes/:id/approve", OweController, :approve_owe
    post "/owes/:id/finish", OweController, :finish_owe
    resources "/friends", FriendController, singleton: true
    resources "/user", UserController, singleton: true
  end

  # Other scopes may use custom stacks.
  # scope "/api", OweAppBackend do
  #   pipe_through :api
  # end
end
