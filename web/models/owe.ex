defmodule OweAppBackend.Owe do
  use OweAppBackend.Web, :model

  schema "owes" do
    belongs_to :from, OweAppBackend.User, references: :facebook_id, type: :string
    belongs_to :to, OweAppBackend.User, references: :facebook_id, type: :string
    field :amount, :integer
    field :approved, :boolean, default: false
    field :finished, :boolean, default: false

    timestamps
  end

  @required_fields ~w(from_id to_id amount approved finished)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
