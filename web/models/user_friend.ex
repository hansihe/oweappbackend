defmodule OweAppBackend.UserFriend do
  use OweAppBackend.Web, :model

  schema "userfriends" do
    field :user_id, :string
    field :friend_id, :string

    timestamps
  end

  @required_fields ~w(user_id friend_id)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
