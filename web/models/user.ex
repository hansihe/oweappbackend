defmodule OweAppBackend.User do
  use OweAppBackend.Web, :model

  @primary_key {:facebook_id, :string, []}
  @derive {Phoenix.Param, key: :facebook_id}
  schema "users" do
    field :full_name, :string
    field :first_name, :string
    field :picture_url, :string

    field :email, :string

    field :access_token, :string

    field :token_ids, {:array, :string}, default: []

    embeds_many :friends, OweAppBackend.User.Friend, on_replace: :delete

    timestamps
  end

  @required_fields ~w(full_name first_name facebook_id email access_token token_ids picture_url)
  @optional_fields ~w(friends)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> unique_constraint(:facebook_id)
  end

end

defmodule OweAppBackend.User.Friend do
  use OweAppBackend.Web, :model

  embedded_schema do
    field :facebook_id
    field :picture_url
    field :name
  end

  @required_fields ~w(facebook_id picture_url name)
  @optional_fields ~w()

  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

end
