defmodule LcArray do

  use Bitwise

  @valueref_null "\x00"
  @valueref_bool_false "\x01"
  @valueref_bool_true "\x02"
  @valueref_number_integer "\x03"
  @valueref_number_double "\x04"
  @valueref_name_empty "\x05"
  @valueref_name_any "\x06"
  @valueref_string_empty "\x07"
  @valueref_string_any "\x08"
  @valueref_data_empty "\x09"
  @valueref_data_any "\x0a"
  @valueref_array_empty "\x0b"
  @valueref_array_sequence "\x0c"
  @valueref_array_map "\x0d"
  
  def decode(data) when is_binary(data) do
  end

  def encode(data) do
    ["\x06", encode_value(data)]
  end

  def encode_value(nil), do: @valueref_null
  def encode_value(false), do: @valueref_bool_false
  def encode_value(true), do:  @valueref_bool_true
  def encode_value(num) when is_integer(num), do: [@valueref_number_integer, <<num::signed-big-integer-size(32)>>]
  def encode_value(num) when is_float(num), do: [@valueref_number_double, <<num::signed-big-float-size(64)>>]

  def encode_value([]), do: @valueref_array_empty
  def encode_value(value) when is_list(value) do
    length = Enum.count(value)
    encoded_values = for item <- value, do: encode_value(item)
    [@valueref_array_sequence, encode_uint4(length), encoded_values]
  end
  def encode_value(value) when is_map(value) do
    map_size = Dict.size(value)
    if map_size == 0 do
      @valueref_array_empty
    else
      encoded_values = for item <- value, do: encode_map_value(item)
      [@valueref_array_map, <<map_size::unsigned-big-integer-size(32)>>, encoded_values]
    end
  end
  def encode_value(value) when is_binary(value) do
    [@valueref_string_any, encode_uint_2or4(byte_size(value)), value]
  end

  def encode_map_value({key, val}) when is_binary(key) do
    [encode_uint_2or4(byte_size(key)), key, encode_value(val)]
  end
  def encode_map_value({atom_key, val}) when is_atom(atom_key) do
    key = Atom.to_string(atom_key)
    encode_map_value({key, val})
  end

  def encode_uint_2or4(value) when is_integer(value) and value >= 0 and value < 16384 do
    <<value::unsigned-big-integer-size(16)>>
  end
  def encode_uint_2or4(value) when is_integer(value) and value >= 16384 do
    <<value::unsigned-big-integer-size(15), 1::integer-size(1), (value >>> 15)::unsigned-big-integer-size(16)>>
  end

  def encode_uint4(value) when is_integer(value) do
    <<value::unsigned-big-integer-size(32)>>
  end

  def test do
    #{:ok, file} = File.open("myfile3.txt", [:write])
    #IO.write file, encode(%{"woo" => "test"})
    #File.close file
    #Base64.
    IO.inspect Base.encode64 IO.iodata_to_binary encode(%{"woo" => "test", "hoo" => %{"too" => "guud", "next" => 12, "prev" => 12.32}, "too" => ["woo", "hoo"]})
  end

end
