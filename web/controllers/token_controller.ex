defmodule OweAppBackend.Token do
  import Joken

  def make(data) do
    token(data)
    |> validate
  end

  def validate(token) do
    token
    |> with_signer(hs256("test"))
    |> with_exp
    |> with_iat
    |> with_nbf
    |> with_validation("exp", &(&1 > current_time))
    |> with_validation("iat", &(&1 <= current_time))
    |> with_validation("nbf", &(&1 <= current_time))
  end

  def encode(payload) do
    make(payload)
    |> sign
    |> get_compact
  end

  def decode(token) do
    make(token)
    |> verify!
  end
end

defmodule OweAppBackend.TokenController do
  use OweAppBackend.Web, :controller
  import OweAppBackend.Router, only: [require_auth: 2, api_response: 2]
  plug :require_auth

  def make_token(user) do
    token_id = UUID.uuid4()
    OweAppBackend.Token.encode(%{
      sub: "facebook_id|" <> user.facebook_id,
      jti: token_id
    })
  end

  def index(conn, _) do
    api_response conn, %{"status" => "0"}
    #text conn, "OK"
  end

  def create(conn, _params) do
    user = conn.assigns.user
    text conn, make_token(user)
  end
end
