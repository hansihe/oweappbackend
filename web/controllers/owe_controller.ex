defmodule OweAppBackend.OweController do
  use OweAppBackend.Web, :controller

  import OweAppBackend.Util

  def type_from_param("owe"), do: :owe
  def type_from_param("owed"), do: :owed
  def user_from_param(user) when is_binary(user), do: user
  def amount_from_param(amount) when is_binary(amount) do
    {amount, ""} = Integer.parse(amount)
    amount
  end

  def parse_create_params(%{"type" => type, "user" => user, "amount" => amount}) do
    %{
      type: type_from_param(type),
      user: user_from_param(user),
      amount: amount_from_param(amount)
    }
  end

  def add_owe(current_user, %{type: :owe} = params) do
    #to_user = OweAppBackend.Repo.get! OweAppBackend.User, params.user
    OweAppBackend.Repo.insert! OweAppBackend.Owe.changeset(%OweAppBackend.Owe{}, %{
      from_id: current_user.facebook_id,
      to_id: params.user,
      amount: params.amount,
      approved: true
    })
  end
  def add_owe(current_user, %{type: :owed} = params) do
    #from_user = OweAppBackend.Repo.get! OweAppBackend.User, params.user
    OweAppBackend.Repo.insert! OweAppBackend.Owe.changeset(%OweAppBackend.Owe{}, %{
      from_id: params.user,
      to_id: current_user.facebook_id,
      amount: params.amount,
      approved: false
    })
  end

  def owe_to_response(owe) do
    IO.inspect {owe.id, owe.approved}
    %{
      id: owe.id,
      approved: owe.approved,
      finished: owe.finished,
      from: %{
        id: owe.from.facebook_id,
        name: owe.from.full_name,
        picture_url: owe.from.picture_url,
      },
      to: %{
        id: owe.to.facebook_id,
        name: owe.to.full_name,
        picture_url: owe.to.picture_url,
      },
      amount: owe.amount
    }
  end

  import OweAppBackend.Router, only: [require_auth: 2, api_response: 2]
  plug :require_auth

  def create(conn, params) do
    parsed_params = parse_create_params(params)
    if Enum.find(conn.assigns.user.friends, 
            fn x -> x.facebook_id == parsed_params.user end) == nil do
      throw "no matching user"
    end

    owe = add_owe(conn.assigns.user, parsed_params)
    owe_preload = OweAppBackend.Repo.preload owe, [:from, :to]
    api_response conn, owe_to_response(owe_preload)
  end

  def index(conn, _params) do
    user_id = conn.assigns.user.facebook_id
    query = from(o in OweAppBackend.Owe,
        where: (o.from_id == ^user_id or o.to_id == ^user_id),
        preload: [:from, :to],
        order_by: [desc: :updated_at])
    owes = OweAppBackend.Repo.all query
    owes_response = Enum.map(owes, &owe_to_response/1)
    api_response conn, owes_response
  end

  def approve_owe(conn, params) do
    {num, ""} = Integer.parse(kw_get(params, "id"))

    owe = OweAppBackend.Owe
    |> OweAppBackend.Repo.get!(num)
    |> OweAppBackend.Repo.preload [:to]
    IO.inspect owe
    IO.inspect conn.assigns.user

    if owe.from_id == conn.assigns.user.facebook_id do
      OweAppBackend.Repo.update!(OweAppBackend.Owe.changeset(owe, %{ approved: true }))
      api_response conn, %{ success: true }
    else
      api_response conn, %{ success: false }
    end
  end

  def finish_owe(conn, params) do
    {num, ""} = Integer.parse(kw_get(params, "id"))
    owe = OweAppBackend.Owe
          |> OweAppBackend.Repo.get!(num)
          |> OweAppBackend.Repo.preload [:to, :from]

    from_id = owe.to.facebook_id
    to_id = owe.from.facebook_id

    case  conn.assigns.user.facebook_id do
      ^to_id ->
        if !owe.approved do
          #OweAppBackend.Repo.delete!(owe)
          OweAppBackend.Repo.update!(
            OweAppBackend.Owe.changeset(owe, %{finished: true}))
          api_response conn, %{ success: true }
        else
          api_response conn, %{ success: false }
        end
      ^from_id ->
        OweAppBackend.Repo.update!(
          OweAppBackend.Owe.changeset(owe, %{finished: true}))
        api_response conn, %{ success: true }
      _ ->
        api_response conn, %{ success: false }
    end
  end

  import Ecto.Query, only: [from: 2]
  #def delete(conn, params) do
    # {id, ""} = Integer.parse(params["id"])
    #owe = OweAppBackend.Repo.get! OweAppBackend.Owe, id
    #OweAppBackend.Repo.delete! owe
    #json conn, %{status: :ok}
    #end
end
