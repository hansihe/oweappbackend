defmodule OweAppBackend.UserController do
  use OweAppBackend.Web, :controller

  import OweAppBackend.Router, only: [require_auth: 2, api_response:  2]
  plug :require_auth

  def show(conn, _params) do
    user = conn.assigns.user
    api_response conn, %{
      id: user.facebook_id,
      picture_url: user.picture_url,
      name: user.full_name,
    }
  end
end
