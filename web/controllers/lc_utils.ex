defmodule OweAppBackend.LcUtilsController do
  use OweAppBackend.Web, :controller

  alias OAuth2.Strategy.Facebook
  alias OweAppBackend.Router.Helpers

  def login_with_callback(conn, _params) do
    redirect(conn, external: Facebook.authorize_url_next!(
        Helpers.lc_utils_path(OweAppBackend.Endpoint, :login_with_callback_finished)))
  end

  import OweAppBackend.TokenController, only: [make_token: 1]
  import OweAppBackend.Router, only: [require_auth:  2]

  plug :require_auth when action in [:login_with_callback_finished]

  def login_with_callback_finished(conn, _params) do
    #text(conn, make_token(conn.assigns.user))
    conn
    |> put_layout(false)
    |> render("callback_finished.html", token: make_token(conn.assigns.user))
  end

end
