defmodule OweAppBackend.LoginController do
  use OweAppBackend.Web, :controller

  alias OAuth2.Strategy.Facebook
  import OweAppBackend.Util

  def ensure_friend(data) do
    if Repo.get(OweAppBackend.User, pair_list_get(data, "id")) == nil do
      Repo.insert %OweAppBackend.User{
        facebook_id: data |> kw_get("id"),
        first_name: data |> kw_get("first_name"),
        full_name: data |> kw_get("name"),
        picture_url: data |> kw_get("picture") |> kw_get("data") |> kw_get("url"),
        email: "",
        access_token: ""}
    end
  end

  def map_friends(user_info) do
    friends_data = (user_info |> kw_get("friends") |> kw_get("data")) || []
    Enum.map(friends_data,
      fn(x) ->
        ensure_friend(x)
        %{
          facebook_id: kw_get(x, "id"),
          picture_url: x |> kw_get("picture") |> kw_get("data") |> kw_get("url"),
          name: kw_get(x, "name"),
        }
      end)
  end

  def insert_user(token, user_info) do
    Repo.insert %OweAppBackend.User{
      facebook_id: user_info |> kw_get("id"),
      first_name: user_info |> kw_get("first_name"),
      full_name: user_info |> kw_get("name"),
      picture_url: user_info |> kw_get("picture") |> kw_get("data") |> kw_get("url"),
      email: "",
      access_token: token,
    }
      #friends: map_friends(user_info)}
  end

  def update_user(user, token, user_info) do
    changeset = OweAppBackend.User.changeset(user, %{
      first_name: user_info |> kw_get("first_name"),
      full_name: user_info |> kw_get("name"),
      picture_url: user_info |> kw_get("picture") |> kw_get("data") |> kw_get("url"),
      access_token: token,
      friends: map_friends(user_info),
    })
    Repo.update changeset
  end

  def upsert_user(token, expires) do
    user_info = OweAppBackend.Facebook.get!(token, "/me",
        %{"fields" =>
            "name,first_name,friends.limit(100){picture,name,first_name},picture"})
    |> OweAppBackend.Facebook.parse_response

    case Repo.get(OweAppBackend.User, kw_get(user_info, "id")) do
      nil -> case insert_user(token, user_info) do
        {:ok, user} -> case update_user(user, token, user_info) do
          {:ok, user} -> user
          {:error, changeset} -> raise changeset
        end
        {:error, changeset} -> raise changeset
      end
      user -> case update_user(user, token, user_info) do
        {:ok, user} -> user
        {:error, changeset} -> raise changeset
      end
    end
  end

  def index(conn, _params) do
    redirect conn, external: Facebook.authorize_url!
  end

  def decode_url("base64_" <> data), do: Base.url_decode64(data)
  def decode_url(data), do: data

  def facebook_return(conn, params) do
    o_token = Facebook.get_token!(code: kw_get(params, "code"))
    %{token: token, expires: expires} = Facebook.get_longlived_token!(o_token)
    user = upsert_user(token, expires)

    conn = conn
    |> Guardian.Plug.sign_in(user)
    |> put_session(:facebook_id, user.facebook_id)

    if kw_get(params, "state") do
      {:ok, decoded} = Base.url_decode64(kw_get(params, "state"))
      redirect(conn, to: decoded)
    else
      text(conn, "woo")
    end
    #if params["next"]
  end
end
