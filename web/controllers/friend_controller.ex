defmodule OweAppBackend.FriendController do
  use OweAppBackend.Web, :controller

  import OweAppBackend.Router, only: [require_auth: 2, api_response: 2]
  plug :require_auth

  def show(conn, _params) do
    friends = for %OweAppBackend.User.Friend{name: name, facebook_id: facebook_id}
        <- conn.assigns.user.friends do
      friend = OweAppBackend.Repo.get!(OweAppBackend.User, facebook_id)
      %{
        "name" => name,
        "id" => facebook_id,
        "picture_url" => friend.picture_url,
      }
    end
    api_response conn, %{"friends" => friends, "status" => "0"}
  end
end
