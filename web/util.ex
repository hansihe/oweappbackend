defmodule OweAppBackend.Util do

  def pair_list_get(list, key) do
    hd(for {k, v} <- list, k == key, do: v)
  end

  def kw_get(nil, _key), do: nil
  def kw_get(list, key) do
    case (for {k, v} <- list, k == key, do: v) do
      [] -> nil
      [value | _] -> value
    end
  end

end
