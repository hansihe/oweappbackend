defmodule OweAppBackend.Facebook do
    
  def request(token, method, path, params \\ %{}) do
    headers = [{"Authorization", "Bearer " <> token}]
    url = "https://graph.facebook.com" <> path <> "?" <> URI.encode_query(params)
    HTTPoison.request(method, url, "", headers)
  end

  def request!(token, method, path, params \\ %{}) do
    case request(token, method, path, params) do
      {:ok, response} -> response
      {:error, error} -> raise error
    end
  end

  def get(token, path, params \\ %{}) do
    request(token, :get, path, params)
  end
  def get!(token, path, params \\ %{}) do
    request!(token, :get, path, params)
  end

  def parse_response(%{status_code: 200, body: body}) do
    Poison.Parser.parse!(body)
  end

end
