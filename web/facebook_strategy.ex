defmodule OAuth2.Strategy.Facebook do
  use OAuth2.Strategy

  def client do
    conf = Application.get_env(:owe_app_backend, :facebook_oauth)
    OAuth2.Client.new([
      strategy: __MODULE__,
      client_id: conf[:client_id],
      client_secret: conf[:client_secret],
      redirect_uri: conf[:redirect_uri],
      site: "https://graph.facebook.com",
      authorize_url: "https://www.facebook.com/dialog/oauth",
      token_url: "https://graph.facebook.com/v2.4/oauth/access_token"
    ])
  end

  def client_redirect_params(params) do
    
  end

  def authorize_url!(params \\ []) do
    client()
    |> put_param(:scope, "")
    |> OAuth2.Client.authorize_url!(params)
  end

  def authorize_url_next!(next) do
    __MODULE__.authorize_url!([state: Base.url_encode64(next)])
  end

  def get_token!(params \\ [], headers \\ [], options \\ []) do
    OAuth2.Client.get_token!(client(), params, headers, options)
  end

  def get_longlived_token!(token) do
    client = client()
    query = URI.encode_query(%{
      "grant_type" => "fb_exchange_token",
      "client_id" => client.client_id,
      "client_secret" => client.client_secret,
      "fb_exchange_token" => token.access_token
    })
    %{body: %{
        "access_token" => new_token,
        "expires" => new_token_expires
      }, 
      status_code: 200} = OAuth2.AccessToken.get!(token, "/oauth/access_token?" <> query)
    %{token: new_token, expires: new_token_expires}
  end

  #Strategy callbacks

  def authorize_url(client, params) do
    OAuth2.Strategy.AuthCode.authorize_url(client, params)
  end

  def get_token(client, params, headers) do
    client
    |> put_header("Accept", "application/json")
    |> OAuth2.Strategy.AuthCode.get_token(params, headers)
  end

end
